class Currency {
  final String code;
  final String name;
  bool isFavorite;
  bool isSelected;

  Currency({
    required this.code,
    required this.name,
    this.isFavorite = false,
    this.isSelected = false,
  });
}

class ExchangeOperation {
  final String fromCurrencyCode;
  final double fromAmount;
  final String toCurrencyCode;
  final double toAmount;
  final DateTime date;

  ExchangeOperation({
    required this.fromCurrencyCode,
    required this.fromAmount,
    required this.toCurrencyCode,
    required this.toAmount,
    required this.date,
  });
}
