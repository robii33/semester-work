class FilterOptions {
  DateTime? startDate;
  DateTime? endDate;
  Set<String> selectedCurrencies;
  FilterPeriod period;

  FilterOptions({
    this.startDate,
    this.endDate,
    Set<String>? selectedCurrencies,
    this.period = FilterPeriod.allTime,
  }) : selectedCurrencies = selectedCurrencies ?? <String>{};
}

enum FilterPeriod { allTime, lastWeek, lastMonth, custom }
