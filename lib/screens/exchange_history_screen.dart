import 'package:flutter/material.dart';
import 'filters_screen.dart';
import '../models/exchange_history.dart';
import '../models/filter_options.dart';
import '../models/currency.dart';

class ExchangeHistoryScreen extends StatefulWidget {
  @override
  _ExchangeHistoryScreenState createState() => _ExchangeHistoryScreenState();
}

class _ExchangeHistoryScreenState extends State<ExchangeHistoryScreen> {
  List<ExchangeOperation> filteredHistory = [];

  Future<void> _openFiltersScreen() async {
    final FilterOptions? result = await Navigator.push(
      context,
      MaterialPageRoute(builder: (context) => FiltersScreen()),
    );
    print(result);
    if (result != null) {
      _applyFilters(result);
    }
  }

  void _applyFilters(FilterOptions options) {
    DateTime now = DateTime.now();
    DateTime startDate = now;
    DateTime endDate = now;

    switch (options.period) {
      case FilterPeriod.lastWeek:
        startDate = now.subtract(Duration(days: 7));
        break;
      case FilterPeriod.lastMonth:
        startDate = now.subtract(Duration(days: 30));
        break;
      case FilterPeriod.custom:
        startDate = options.startDate!;
        endDate = options.endDate!;
        break;
      case FilterPeriod.allTime:
      default:
        startDate = DateTime(2000); // Примерная дата начала для "за все время"
        endDate = now;
        break;
    }

    // Фильтрация exchangeHistory по выбранному периоду и валютам
    List<ExchangeOperation> tempFilteredHistory =
        exchangeHistory.where((operation) {
      bool isDateInRange =
          operation.date.isAfter(startDate) && operation.date.isBefore(endDate);
      bool isCurrencySelected = options.selectedCurrencies.isEmpty ||
          options.selectedCurrencies.contains(operation.fromCurrencyCode) ||
          options.selectedCurrencies.contains(operation.toCurrencyCode);
      return isDateInRange && isCurrencySelected;
    }).toList();

    setState(() {
      filteredHistory = tempFilteredHistory;
    });
  }

  @override
  void initState() {
    super.initState();
    // Инициализация filteredHistory всей историей при первом запуске
    filteredHistory = List.from(exchangeHistory);
  }

  @override
  Widget build(BuildContext context) {
    exchangeHistory
        .sort((a, b) => b.date.compareTo(a.date)); // Сортируем по дате

    return Scaffold(
      appBar: AppBar(
        title: Text("История операций"),
        actions: [
          IconButton(
            icon: Icon(Icons.filter_list),
            onPressed: _openFiltersScreen,
          ),
        ],
      ),
      body: ListView.builder(
        itemCount: filteredHistory.length,
        itemBuilder: (context, index) {
          final operation = filteredHistory[index];
          return ListTile(
            title: Text(
                "${operation.fromCurrencyCode} -> ${operation.toCurrencyCode}"),
            subtitle: Text(
                "${operation.fromAmount} -> ${operation.toAmount}, Дата: ${operation.date}"),
          );
        },
      ),
    );
  }
}
