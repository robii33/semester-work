import 'package:flutter/material.dart';
import '../models/currency.dart';
import '../models/exchange_history.dart';

class CurrencyExchangeScreen extends StatefulWidget {
  final Currency fromCurrency;
  final Currency toCurrency;

  CurrencyExchangeScreen(
      {required this.fromCurrency, required this.toCurrency});

  @override
  _CurrencyExchangeScreenState createState() => _CurrencyExchangeScreenState();
}

class _CurrencyExchangeScreenState extends State<CurrencyExchangeScreen> {
  final TextEditingController _fromAmountController =
      TextEditingController(text: "1");
  final TextEditingController _toAmountController = TextEditingController();

  double getExchangeRate(Currency from, Currency to) {
    Map<String, double> rates = {
      'USDEUR': 0.95,
      'EURUSD': 1.05,
      'USDRUB': 75.0,
      'RUBUSD': 0.013,
      'EURRUB': 80.0,
      'RUBEUR': 0.0125,
    };

    String key = '${from.code}${to.code}';
    return rates[key] ?? 1.0; // Возвращаем 1.0, если конкретный курс не найден
  }

  @override
  void initState() {
    super.initState();
    _toAmountController.text =
        (1.0 * getExchangeRate(widget.fromCurrency, widget.toCurrency))
            .toStringAsFixed(2);
  }

  @override
  void dispose() {
    // Очищение контроллеров при удалении виджета из дерева виджетов
    _fromAmountController.dispose();
    _toAmountController.dispose();
    super.dispose();
  }

  // Функция для сохранения операции обмена в историю
  void performExchange() {
    // Получаем количество валют из контроллеров
    final double fromAmount = double.tryParse(_fromAmountController.text) ?? 0;
    final double toAmount = double.tryParse(_toAmountController.text) ?? 0;

    // Создаем операцию обмена
    ExchangeOperation operation = ExchangeOperation(
      fromCurrencyCode: widget.fromCurrency.code,
      fromAmount: fromAmount,
      toCurrencyCode: widget.toCurrency.code,
      toAmount: toAmount,
      date: DateTime.now(),
    );

    // Добавляем операцию в историю
    exchangeHistory.add(operation);

    // Возвращаемся на экран списка валют
    Navigator.pop(context);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Обмен валют"),
      ),
      body: Padding(
        padding: const EdgeInsets.all(8.0),
        child: Column(
          children: [
            TextField(
              controller: _fromAmountController,
              decoration: InputDecoration(
                labelText: 'Количество ${widget.fromCurrency.code}',
              ),
              keyboardType: TextInputType.numberWithOptions(decimal: true),
              onChanged: (value) {
                final double fromValue = double.tryParse(value) ?? 0;
                _toAmountController.text = (fromValue *
                        getExchangeRate(widget.fromCurrency, widget.toCurrency))
                    .toStringAsFixed(2);
              },
            ),
            SizedBox(height: 20),
            TextField(
              controller: _toAmountController,
              decoration: InputDecoration(
                labelText: 'Количество ${widget.toCurrency.code}',
              ),
              keyboardType: TextInputType.numberWithOptions(decimal: true),
              onChanged: (value) {
                final double toValue = double.tryParse(value) ?? 0;
                _fromAmountController.text = (toValue /
                        getExchangeRate(widget.fromCurrency, widget.toCurrency))
                    .toStringAsFixed(2);
              },
            ),
            SizedBox(height: 20),
            ElevatedButton(
              onPressed: performExchange,
              child: Text("Обменять"),
            ),
          ],
        ),
      ),
    );
  }
}
