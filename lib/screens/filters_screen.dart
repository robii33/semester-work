import 'package:flutter/material.dart';
import '../models/filter_options.dart';

class FiltersScreen extends StatefulWidget {
  @override
  _FiltersScreenState createState() => _FiltersScreenState();
}

class _FiltersScreenState extends State<FiltersScreen> {
  FilterOptions _filterOptions = FilterOptions();
  DateTime _today = DateTime.now();

  List<String> currenciesInHistory = ['USD', 'EUR', 'RUB'];

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Фильтры"),
      ),
      body: SingleChildScrollView(
        child: Column(
          children: [
            ListTile(
              title: Text("За все время"),
              leading: Radio<FilterPeriod>(
                value: FilterPeriod.allTime,
                groupValue: _filterOptions.period,
                onChanged: (FilterPeriod? value) {
                  setState(() {
                    _filterOptions.period = value!;
                  });
                },
              ),
            ),
            ListTile(
              title: Text("За последнюю неделю"),
              leading: Radio<FilterPeriod>(
                value: FilterPeriod.lastWeek,
                groupValue: _filterOptions.period,
                onChanged: (FilterPeriod? value) {
                  setState(() {
                    _filterOptions.period = value!;
                  });
                },
              ),
            ),
            ListTile(
              title: Text("За последний месяц"),
              leading: Radio<FilterPeriod>(
                value: FilterPeriod.lastMonth,
                groupValue: _filterOptions.period,
                onChanged: (FilterPeriod? value) {
                  setState(() {
                    _filterOptions.period = value!;
                  });
                },
              ),
            ),

            // Чекбоксы для выбора валют
            ...currenciesInHistory
                .map((currency) => CheckboxListTile(
                      title: Text(currency),
                      value:
                          _filterOptions.selectedCurrencies.contains(currency),
                      onChanged: (bool? selected) {
                        setState(() {
                          if (selected!) {
                            _filterOptions.selectedCurrencies.add(currency);
                          } else {
                            _filterOptions.selectedCurrencies.remove(currency);
                          }
                        });
                      },
                    ))
                .toList(),

            ElevatedButton(
              onPressed: () {
                Navigator.pop(context, _filterOptions);
              },
              child: Text("Применить"),
            ),
          ],
        ),
      ),
    );
  }
}
