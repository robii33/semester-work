import 'package:flutter/material.dart';
import '../models/currency.dart';
import 'currency_exchange_screen.dart';
import 'exchange_history_screen.dart';

class CurrencyListScreen extends StatefulWidget {
  @override
  _CurrencyListScreenState createState() => _CurrencyListScreenState();
}

class _CurrencyListScreenState extends State<CurrencyListScreen> {
  List<Currency> currencies = [
    Currency(code: "USD", name: "USD", isFavorite: false, isSelected: false),
    Currency(code: "EUR", name: "EUR", isFavorite: false, isSelected: false),
    Currency(code: "RUB", name: "RUB", isFavorite: false, isSelected: false),
  ];
  Currency? selectedCurrency; // Для отслеживания выбранной валюты

  @override
  Widget build(BuildContext context) {
    List<Currency> displayedCurrencies = currencies;
    // Сортировка валют: сначала избранные, затем остальные
    currencies.sort((a, b) {
      if (a.isSelected && !b.isSelected) return -1;
      if (!a.isSelected && b.isSelected) return 1;
      if (a.isFavorite && !b.isFavorite) return -1;
      if (!a.isFavorite && b.isFavorite) return 1;
      return a.name.compareTo(b.name);
    });

    return Scaffold(
      appBar: AppBar(
        title: Text("Список валют"),
        actions: <Widget>[
          IconButton(
            icon: Icon(Icons.history),
            onPressed: () {
              // Навигация к экрану истории обмена
              Navigator.push(
                context,
                MaterialPageRoute(
                    builder: (context) => ExchangeHistoryScreen()),
              );
            },
          ),
        ],
      ),
      body: Column(
        children: [
          Expanded(
            child: ListView.builder(
              itemCount: displayedCurrencies.length,
              itemBuilder: (context, index) {
                Currency currency = displayedCurrencies[index];
                return ListTile(
                  title: Text(currency.name),
                  trailing: IconButton(
                    icon: Icon(
                      currency.isFavorite ? Icons.star : Icons.star_border,
                      color: currency.isFavorite ? Colors.amber : null,
                    ),
                    onPressed: () {
                      setState(() {
                        currency.isFavorite = !currency.isFavorite;
                      });
                    },
                  ),
                  leading: currency.isSelected
                      ? Icon(Icons.check_circle_outline)
                      : null, // Добавляем индикатор для выделенной валюты
                  onTap: () {
                    if (selectedCurrency == null) {
                      // Показываем SnackBar, если валюта не выбрана
                      ScaffoldMessenger.of(context).showSnackBar(const SnackBar(
                        content: Text(
                            "Пожалуйста, выберите валюту долгим нажатием и затем выберите вторую валюту"),
                        duration: Duration(seconds: 5),
                      ));
                    } else if (selectedCurrency != null &&
                        selectedCurrency != currency) {
                      // Если валюта выбрана, переходим на страницу обмена
                      Navigator.push(
                        context,
                        MaterialPageRoute(
                          builder: (context) => CurrencyExchangeScreen(
                            fromCurrency: selectedCurrency!,
                            toCurrency: currency,
                          ),
                        ),
                      );
                    }
                  },
                  onLongPress: () => setState(() {
                    // Проверяем, выделена ли уже эта валюта
                    if (currency.isSelected) {
                      // Если да, то снимаем выделение
                      currency.isSelected = false;
                      selectedCurrency = null; // Сбрасываем выбранную валюту
                    } else {
                      // Сбрасываем выделение для всех валют
                      for (var curr in currencies) {
                        curr.isSelected = false;
                      }
                      // Выделяем текущую валюту
                      currency.isSelected = true;
                      selectedCurrency = currency;
                    }
                  }),
                );
              },
            ),
          ),
        ],
      ),
    );
  }
}
